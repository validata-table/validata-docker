# validata-docker

Run Validata with Docker.

## Requirements

First install [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/) if not already done.

## Run on development machine

```bash
git clone https://gitlab.com/validata-table/validata-api.git
git clone https://gitlab.com/validata-table/validata-ui.git
git clone https://gitlab.com/validata-table/validata-docker.git
cd validata-docker/development
docker-compose up -d
```

This will bring up both Validata UI (http://localhost:5000/) and Validata Web API (http://localhost:5001/).

Configuration is done by editing environment variables in [api.env](./development/api.env) and [ui.env](./development/ui.env). Warning: Docker env files do not support using quotes around variable values!

Don't forget to use `docker-compose restart` to update.

## Deploy to production

### Install requirements

```bash
cd production
ansible-galaxy install -r requirements.yml
```

### Install Validata on server

The following depends on your server provider. Here are the instructions used by a server hosted at Scaleway:

```bash
cd production
export SCW_TOKEN="..." # secret from Jailbrak KeePassX passwords file
ansible-playbook --inventory scaleway_inventory.yml --limit validata --user root playbooks/validata.yml
```

Then copy [api.secrets.env.template](./production/api.secrets.env.template) and [ui.secrets.env.template](./production/ui.secrets.env.template) to `/etc/docker/compose/validata` on server (removing the `.template` extension), and fill-in the values. Read [validata-api](https://gitlab.com/validata-table/validata-api) and [validata-ui](https://gitlab.com/validata-table/validata-ui) documentation for more information about environment variables.

The installed version uses Validata API `v0.2.0` and Validata UI `v0.2.0`. Follow the next section to update to a new version.

### Configuration

#### Homepage config

To customize Validata UI homepage, modify `go.validata.fr/homepage_config.json`.

### Deploy a new version

To deploy a new version of Validata:

```bash
ssh root@beta.validata.fr
cd /etc/docker/compose/validata
```

Edit `docker-compose.yml` to update the image tags of `api` and `ui` services, according to the latest version you want to deploy. After saving the file:

```bash
docker-compose pull
docker-compose up -d
```

Test that it works. To see the logs:

```bash
docker-compose logs
docker-compose logs -f
```

If it fails you can rollback to the previous version by editing `docker-compose.yml` and changing Docker image tags, then:

```bash
docker-compose up -d
```

If you want to free disk space, you can prune old Docker objects (images, containers, etc.):

```bash
docker images
docker system prune
```

## See also

- https://gitlab.com/validata-table/validata-ui
- https://gitlab.com/validata-table/validata-api
